<?php

namespace App\Tests\Service;

use App\Service\fit;
use PHPUnit\Framework\TestCase;
use App\Entity\GdfFitDatas;

class GdfFitDatasTest extends TestCase {
    public function testfetchFitByFid(){
        
        $fit = new fit();
        $result = $fit->fetchFitByFid("802");
        $this->assertEquals("802", $result->response->fid);
    }

}
